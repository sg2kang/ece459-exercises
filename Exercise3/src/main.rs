use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for i in 0..N {
        children.push(thread::spawn(move || {
            thread::sleep(Duration::from_millis(1));
            println!("Thread {} completed!", i);
        }));
    }

    for child in children {
        child.join().unwrap();
    }
    println!("Main thread done");
}
