use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 10;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    for i in 0..N {
        let tx = mpsc::Sender::clone(&tx);
        thread::spawn(move || {
            let msg = format!("Thread {}", i);
            tx.send(msg).unwrap();
            thread::sleep(Duration::from_secs(1));
            println!("Thread {} done", i);
        });
    }

    for received in rx {
        println!("Got: {}", received);
    }
}
