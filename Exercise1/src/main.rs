// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let mut sum = 0;

    for x in 1..((number - 1) / multiple1 + 1) {
        sum += multiple1 * x;
    }

    for x in 1..((number - 1) / multiple2 + 1) {
        sum += multiple2 * x;
    }

    let lcm = multiple1 / gcd(multiple1, multiple2) * multiple2;

    for x in 1..((number - 1) / lcm + 1) {
        sum -= lcm * x;
    }

    return sum;
}

fn gcd(a: i32, b: i32) -> i32 {
    let mut a = a;
    let mut b = b;
    let mut t;
    while b != 0 {
        t = b;
        b = a % b;
        a = t;
    }
    return a;
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
