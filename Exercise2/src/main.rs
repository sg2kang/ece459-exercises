// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let n = n as usize;
    let mut dp = vec![0; n + 1];
    dp[1] = 1;

    for i in 2..n + 1 {
        dp[i] = dp[i - 1] + dp[i - 2];
    }

    return dp[n];
}


fn main() {
    println!("{}", fibonacci_number(10));
}
